from ..model.schema import Schema
from ..model.schema import Column

staff_schema = Schema('staff_view', [
    Column('职员姓名', 'varchar', 'staff_name', 50),
    Column('拼音姓名', 'varchar', 'staff_pinyin', 50),
    Column('性别', 'varchar', 'staff_gender', 4),
    Column('职员编码', 'varchar', 'staff_code', 8),
    Column('职位名称', 'varchar', 'staff_position', 255),
    Column('职称资格', 'varchar', 'staff_title', 255),
    Column('办公室电话', 'varchar', 'staff_phone', 20),
    Column('参加工作时间', 'datetime', 'staff_since'),
    Column('涉密等级', 'varchar', 'staff_level', 10),
    Column('员工类型', 'varchar', 'staff_present', 4),
    Column('身份证号', 'varchar', 'staff_id', 40),
    Column('部门代码', 'varchar', 'staff_section', 8),
])
