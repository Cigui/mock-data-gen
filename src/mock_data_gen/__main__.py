from faker import Faker
import mysql.connector

from .schema.staff import staff_schema
from .provider.staff import StaffProvider
from .config import config_object
from .sql import do_create, do_insert

if __name__ == "__main__":
    # initialize Faker object
    fake = Faker()
    # add customized provider to define the data model
    fake.add_provider(StaffProvider)

    # connect to mysql
    db = mysql.connector.connect(host=config_object.MYSQL_HOST,
                                 user=config_object.MYSQL_USER,
                                 port=config_object.MYSQL_PORT,
                                 passwd=config_object.MYSQL_PASSWORD,
                                 db=config_object.MYSQL_DB)
    cursor = db.cursor()

    # create schema & insert mock data
    do_create(cursor, staff_schema)
    do_insert(cursor, staff_schema, [fake.staff() for i in range(3000)])

    db.commit()
