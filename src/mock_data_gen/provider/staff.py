from faker.providers import BaseProvider
from faker.providers.bank import Provider as BankProvider
from faker.providers.date_time import Provider as DateProvider
from faker.providers.job.zh_CN import Provider as JobProvider
from faker.providers.misc import Provider as MiscProvider
from faker.providers.person.zh_CN import Provider as PersonProvider
from faker.providers.ssn.zh_CN import Provider as SsnProvider
from pypinyin import lazy_pinyin, Style

from ..schema.staff import staff_schema


class StaffProvider(BankProvider, DateProvider, JobProvider, MiscProvider,
                    PersonProvider, SsnProvider, BaseProvider):
    def staff_name(self):
        self.__name = self.name()
        return self.__name

    def staff_pinyin(self):
        return ''.join(
            lazy_pinyin(self.__name, strict=False, style=Style.NORMAL))

    def staff_gender(self):
        return self.random_element(elements=('0', '1'))

    def staff_code(self):
        return self.swift8()

    def staff_position(self):
        self.__job = self.job()
        return self.__job

    def staff_title(self):
        return self.__job

    def staff_phone(self):
        return self.numerify(self.random_element(("########", )))

    def staff_since(self):
        return self.date_between().strftime('%Y-%m-%d')

    def staff_level(self):
        return self.random_element(elements=('不涉密', '一般', '重要', '核心'))

    def staff_present(self):
        return self.random_element(elements=('在职', '聘用', '返聘', '其他'))

    def staff_id(self):
        return self.ssn()

    def staff_locked(self):
        return self.random_element(elements=(0, 1))

    def staff_section(self):
        return self.swift8()

    def staff(self):
        return [
            f"{getattr(self, col.faker_method)()}"
            for col in staff_schema.columns
        ]
