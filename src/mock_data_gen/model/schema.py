from typing import List

from ..model.column import Column


class Schema(object):
    def __init__(self, name: str, columns: List[Column]):
        self.name = name
        self.columns = columns
