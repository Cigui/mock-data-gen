class Column(object):
    def __init__(self,
                 name: str,
                 type_: str,
                 faker_method: str,
                 size: int = None):
        self.name = name
        self.type = type_
        self.faker_method = faker_method
        self.size = size
