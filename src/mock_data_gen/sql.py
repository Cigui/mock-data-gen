from mysql.connector.cursor import MySQLCursor

from .model.schema import Schema
from .model.column import Column


def column_to_sql(col: Column):
    return f'`{col.name}` {col.type}{f"({col.size})" if col.size is not None else ""}'


def create_table(schema: Schema):
    return f"CREATE TABLE `{schema.name}` ({', '.join([column_to_sql(col) for col in schema.columns])})"


def do_create(cursor: MySQLCursor, schema: Schema):
    cursor.execute(create_table(schema))


def do_insert(cursor: MySQLCursor, schema: Schema, values):
    cursor.executemany(
        f"INSERT INTO `{schema.name}` ({', '.join(f'`{x.name}`' for x in schema.columns)})"
        + f"VALUES ({', '.join(['%s' for _ in range(len(schema.columns))])})",
        values)
